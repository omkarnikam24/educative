package src.com.educative.ds.java.stackqueue;

public class Tester {

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>(10);
        stack.push(4);
        stack.push(6);
        stack.push(3);
        stack.push(2);
        stack.push(8);
        stack.push(1);

        //System.out.println(stack.getMaxSize());
        //System.out.println(stack.isEmpty());
        //System.out.println(stack.isFull());

        Queue<Integer> queue = new Queue<>(10);
        //System.out.println(queue.isEmpty());
        //System.out.println(queue.isFull());

        //Generate Binary Test
        GenerateBinary.findBin(10);

        // Reverse K Elements Test
        /*for (int i = 1; i <= 10; i++) {
            queue.enqueue(i);
        }
        ReverseKElements.reverseK(queue, 5);
        while(!queue.isEmpty())
            System.out.println(queue.dequeue());*/

        //Next Greater Element
        //int arr[] = {4, 6, 3, 2, 8, 1};
        //System.out.println(NextGreaterElement.nextGreaterElement(arr));

        // Celebrity Problem
        int [][] party1 = {
                {0,1,1,0},
                {1,0,1,1},
                {0,0,0,0},
                {0,1,1,0},
        };
        System.out.println("Celebrity is : " + CelebrityProblem.findCelebrity(party1, 4));

        // Balanced Parentheses
        System.out.println(BalancedParentheses.isBalanced("{[()]}"));
    }

}
