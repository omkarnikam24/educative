package src.com.educative.ds.java.stackqueue;

/**
 * Challenge 1: Generate Binary Numbers from 1 to n using a Queue
 */
public class GenerateBinary {

    public static String[] findBin(int number) {
        String[] result = new String[number];
        Queue<String> queue = new Queue<>(number + 1);

        queue.enqueue("1");
        for (int i = 0; i < number; i++) {
            result[i] = queue.dequeue();
            String s1 = result[i] + "0";
            String s2 = result[i] + "1";
            queue.enqueue(s1);
            queue.enqueue(s2);
        }
        for (String s: result) {
            System.out.print(s + " ");
        }
        return result;
    }
}
