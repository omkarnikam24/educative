package src.com.educative.ds.java.stackqueue;

public class Queue<T> {

    private int maxSize;
    private T[] arr;
    private int front;
    private int back;
    private int currentSize;

    public Queue(int maxSize) {
        this.maxSize = maxSize;
        arr = (T[])new Object[maxSize];
        front = 0;
        back = -1;
        currentSize = 0;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public int getCurrentSize() {
        return currentSize;
    }

    public boolean isEmpty() {
        return currentSize == 0;
    }

    public boolean isFull() {
        return maxSize == currentSize;
    }

    public T top() {
        if(isEmpty())
            return null;
        return arr[front];
    }

    public void enqueue(T data) {
        if(isFull())
            return;
        back = (back + 1) % maxSize;
        arr[back] = data;
        currentSize++;
    }

    public T dequeue() {
        if(isEmpty())
            return null;
        T temp = arr[front];
        front = (front + 1) % maxSize;
        currentSize--;
        return temp;
    }
}
