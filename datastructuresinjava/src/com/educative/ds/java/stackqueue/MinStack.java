package src.com.educative.ds.java.stackqueue;

/**
 * Challenge 10: Create Stack where min() gives minimum in O(1)
 */
public class MinStack {

    private int maxSize;
    private Stack<Integer> mainStack;
    private Stack<Integer> minStack;

    public MinStack(int maxSize) {
        this.maxSize = maxSize;
        this.mainStack = new Stack<>(maxSize);
        this.minStack = new Stack<>(maxSize);
    }

    public int pop() {
        minStack.pop();
        return mainStack.pop();
    }

    public void push(int data) {
        mainStack.push(data);
        if(!minStack.isEmpty() && minStack.top() < data)
            minStack.push(minStack.top());
        else
            minStack.push(data);
    }

    public int min() {
        return minStack.top();
    }
}
