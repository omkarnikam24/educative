package src.com.educative.ds.java.stackqueue;

/**
 * Challenge 9: Check for Balanced Parentheses using a Stack
 * In this problem, you have to implement the isBalanced() method, which will take a string containing
 * only curly {}, square [], and round () parentheses. The method will tell us whether all the parentheses
 * in the string are balanced or not.
 */
public class BalancedParentheses {

    public static boolean isBalanced(String exp) {

        Stack<Character> stack = new Stack<>(exp.length());
        for (int i = 0; i < exp.length(); i++) {
            char c = exp.charAt(i);
            if(c == '}' || c == ')' || c == ']') {
                if(stack.isEmpty())
                    return false;
                if((c == '}' && stack.pop() != '{') || (c == ')' && stack.pop() != '(') || (c == ']' && stack.pop() != '['))
                    return false;
            }
            else stack.push(c);
        }
        if(!stack.isEmpty())
            return false;
        return true;
    }
}
