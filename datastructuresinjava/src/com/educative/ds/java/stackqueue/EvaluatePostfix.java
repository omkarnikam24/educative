package src.com.educative.ds.java.stackqueue;

/**
 * Challenge 6: Evaluate Postfix Expressions using Stacks
 */
public class EvaluatePostfix {

    public static int evaluatePostFix(String expression) {
        char[] array = expression.toCharArray();
        Stack<Integer> stack = new Stack<>(expression.length());
        for (int i = 0; i < expression.length(); i++) {
            char c = expression.charAt(i);
            if(!Character.isDigit(c)){
                int x = stack.pop();
                int y = stack.pop();
                switch (c) {
                    case '+':
                        stack.push(y + x);
                        break;
                    case '-':
                        stack.push(y - x);
                        break;
                    case '/':
                        stack.push(y / x);
                        break;
                    case '*':
                        stack.push(y * x);
                        break;
                }
            } else
                stack.push(Character.getNumericValue(c));
        }
        return stack.pop();
    }
}
