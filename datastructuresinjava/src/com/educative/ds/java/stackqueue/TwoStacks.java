package src.com.educative.ds.java.stackqueue;

/**
 * Challenge 2: Implement Two Stacks using one Array
 */
public class TwoStacks<T> {

    private int maxSize;
    private int top1, top2;
    private T[] arr;

    public TwoStacks(int maxSize) {
        this.maxSize = maxSize;
        top1 = -1;
        top2 = maxSize;
        arr = (T[]) new Object[maxSize];
    }

    //insert at top of first stack
    public void push1(T value) {
        if(top1 < top2 - 1)
            arr[++top1] = value;
    }

    //insert at top of second stack
    public void push2(T value) {
        if(top1 < top2 - 1)
            arr[--top2] = value;
    }

    //remove and return value from top of first stack
    public T pop1() {
        if(top1 > -1)
            return arr[top1--];
        return null;
    }

    //remove and return value from top of second stack
    public T pop2() {
        if(top2 < maxSize)
            return arr[top2++];
        return null;
    }
}