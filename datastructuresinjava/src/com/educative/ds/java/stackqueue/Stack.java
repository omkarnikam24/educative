package src.com.educative.ds.java.stackqueue;

public class Stack<T> {

    private int maxSize;
    private int top;
    private T arr[];

    public Stack(int maxSize) {
        this.maxSize = maxSize;
        this.top = -1;
        this.arr = (T[]) new Object[maxSize];
    }

    public int getMaxSize() {
        return maxSize;
    }

    public boolean isEmpty() {
        return top == -1;
    }

    public boolean isFull() {
        return top == maxSize - 1;
    }

    public T top() {
        if (isEmpty())
            return null;
        return arr[top];
    }

    public void push(T data) {
        if (isFull()) {
            System.err.println("Stack is Full!");
            return;
        }
        arr[++top] = data;
    }

    public T pop() {
        if (isEmpty())
            return null;
        return arr[top--];
    }

}
