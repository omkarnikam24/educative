package src.com.educative.ds.java.stackqueue;

/**
 * Challenge 8: Solve a Celebrity Problem using a Stack
 * In this problem, you have to implement findCelebrity() method to find the celebrity in a party (matrix)
 * using a stack. A celebrity is someone that everyone knows, but he/she doesn’t know anyone at the party.
 */
public class CelebrityProblem {

    public static int findCelebrity(int[][] party, int numPeople) {
        Stack<Integer> stack = new Stack<>(numPeople);
        int celebrity = -1;
        for (int i = 0; i < numPeople; i++) {
            stack.push(i);
        }
        while (!stack.isEmpty()) {
            int x = stack.pop();
            if (stack.isEmpty()) {
                celebrity = x;
                break;
            }
            int y = stack.pop();
            if (aqStatus(party, x, y)) {
                stack.push(y);
            } else
                stack.push(x);
        }
        for (int i = 0; i < numPeople; i++) {
            if (celebrity != i && (aqStatus(party, celebrity, i) || !(aqStatus(party, i, celebrity))))
                return -1;
        }
        return celebrity;
    }

    private static boolean aqStatus(int[][] party, int x, int y) {
        if (party[x][y] == 1)
            return true;
        return false;
    }
}
