package src.com.educative.ds.java.stackqueue;

/**
 * Challenge 7: Next Greater Element using Stack
 */
public class NextGreaterElement {

    public static int[] nextGreaterElement(int[] arr) {
        int[] result = new int[arr.length];
        Stack<Integer> stack = new Stack<>(arr.length);

        for (int i = arr.length - 1; i >= 0 ; i--) {
            if(!stack.isEmpty()) {
                while(!stack.isEmpty() && stack.top() <= arr[i]) {
                    stack.pop();
                }
            }
            if(stack.isEmpty())
                result[i] = -1;
            else
                result[i] = stack.top();
            stack.push(arr[i]);
        }
        return result;
    }
}
