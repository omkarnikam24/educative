package src.com.educative.ds.java.stackqueue;

/**
 * Challenge 3: Reversing the First k Elements of a Queue
 */
public class ReverseKElements {

    public static <T> void reverseK(Queue<T> queue, int k) {
        if(queue.isEmpty() || k <= 0)
            return;
        Stack<T> stack = new Stack<T>(k);
        while(!stack.isFull())
            stack.push(queue.dequeue());
        while(!stack.isEmpty())
            queue.enqueue(stack.pop());
        for (int i = 0; i < queue.getCurrentSize() - k; i++) {
            queue.enqueue(queue.dequeue());
        }
    }
}
