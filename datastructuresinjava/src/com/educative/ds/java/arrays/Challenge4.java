package src.com.educative.ds.java.arrays;

/**
 * Challenge 4: Array of Products of All Elements Except Itself
 * Given an array, return an array where each index stores the product of all numbers except the number on the index itself. Implement your solution in Java and see if your output matches the expected output!
 */
public class Challenge4 {

    private static int[] findProduct(int[] arr) {
        int[] result = new int[arr.length];
        int zeroCount = 0;
        int zeroIndex = -1;
        int product = 1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                zeroCount++;
                zeroIndex = i;
            }
        }
        if (zeroCount > 1) {
            for (int i = 0; i < arr.length; i++) {
                result[i] = 0;
            }
        } else {
            for (int i = 0; i < arr.length; i++) {
                if (i != zeroIndex)
                    product *= arr[i];
            }

            for (int i = 0; i < arr.length; i++) {
                if (zeroCount == 0) {
                    result[i] = product / arr[i];
                } else if (zeroIndex != i) {
                    result[i] = 0;
                } else
                    result[i] = product;
            }
        }
        for (int i : result) System.out.println(i);
        return result;
    }

    public static void main(String[] args) {
        int[] arr = {2,5,9,3,6};
        //int[] arr = {4, 2, 1, 5, 0};
        findProduct(arr);
    }
}
