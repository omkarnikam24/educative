package src.com.educative.ds.java.arrays;

import java.util.HashMap;
import java.util.Hashtable;

/**
 * Challenge 6: First Non-Repeating Integer in an Array
 * Given an array, find the first integer, which is unique in the array. Unique means the number does not repeat and appears only once in the whole array.
 * Implement your solution in Java and see if it runs correctly!
 */
public class Challenge6 {

    private static int findFirstUnique(int[] arr) {

        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            if(map.get(arr[i]) != null )
                map.put(arr[i], map.get(arr[i]) + 1);
            else
                map.put(arr[i], 1);
        }

        for (int i : arr) {
            if (map.get(i) == 1)
                return i;
        }

        return -1;
    }

    public static void main(String[] args) {
        int[] arr = {9, 2, 3, 2, 6, 6};
        System.out.println(findFirstUnique(arr));
    }
}
