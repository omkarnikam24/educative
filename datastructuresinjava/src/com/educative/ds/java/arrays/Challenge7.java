package src.com.educative.ds.java.arrays;

/**
 * Challenge 7: Find Second Maximum Value in an Array
 * Given an array of size n, can you find the second maximum element in the array? Implement your solution in Java and see if your output matches the correct output!
 */
public class Challenge7 {

    private static int findSecondMaximum(int[] arr) {
        int max = Integer.MIN_VALUE;
        int secondMax = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                secondMax = max;
                max = arr[i];
            } else if (arr[i] > secondMax)
                secondMax = arr[i];
        }
        return secondMax;
    }

    public static void main(String[] args) {
        int[] arr = {4, 2, 1, 5, 0};
        System.out.println(findSecondMaximum(arr));
    }
}
