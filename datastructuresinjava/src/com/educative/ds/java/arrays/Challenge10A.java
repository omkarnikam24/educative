package src.com.educative.ds.java.arrays;

/**
 * Challenge 10: Rearrange Sorted Array in Max/Min Form
 * Given an array, can you re-arrange the elements such that the first position will have the largest number,
 * the second will have the smallest, the third will have the second-largest, and so on.
 * Implement your solution in Java and see if your code runs successfully!
 * Note: The given array is sorted in ascending order.
 * Note: The range of integers in the array can be from 0 to 10000.
 */
public class Challenge10A {

    private static void maxMin(int[] arr) {
        int length = arr.length;
        int[] result = new int[length];
        int i = 0;
        int k = 0;
        while(i <= length - i - 1) {
            if(i == length - i - 1) {
                result[k] = arr[i];
                break;
            }
            result[k++] = arr[length - i - 1];
            result[k++] = arr[i];
            i++;
        }

        // The result arr has been copied to the original array, as it was a requirement of the challenge.
        for (int j = 0; j < arr.length; j++) {
            arr[j] = result[j];
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        maxMin(arr);
    }
}
