package src.com.educative.ds.java.arrays;

/**
 * Challenge 2: Merge Two Sorted Arrays
 * Given two sorted arrays, merge them into one array, which should also be sorted. Implement the solution in Java and see if your code runs successfully!
 */
public class Challenge2 {

    private static int[] mergeArrays(int[] arr1, int[] arr2) {
        int s1 = arr1.length;
        int s2 = arr2.length;
        int[] result = new int[s1 + s2];
        int i = 0, j = 0, k = 0;
        while (i < s1 && j < s2) {
            if (arr1[i] < arr2[j])
                result[k++] = arr1[i++];
            else
                result[k++] = arr2[j++];
        }
        while (i < s1)
            result[k++] = arr1[i++];
        while (j < s2)
            result[k++] = arr2[j++];
        return result;
    }

    public static void main(String[] args) {
        int[] arr1 = {1, 3, 4, 5};
        int[] arr2 = {2, 6, 7, 8};
        mergeArrays(arr1, arr2);
    }
}
