package src.com.educative.ds.java.arrays;

/**
 * Challenge 10: Rearrange Sorted Array in Max/Min Form
 * Given an array, can you re-arrange the elements such that the first position will have the largest number,
 * the second will have the smallest, the third will have the second-largest, and so on.
 * Implement your solution in Java and see if your code runs successfully!
 * Note: The given array is sorted in ascending order.
 * Note: The range of integers in the array can be from 0 to 10000.
 */
public class Challenge10B {

    private static void maxMin(int[] arr) {
        int maxId = arr.length - 1;
        int minId = 0;
        int maxNumber = arr[maxId] + 1;
        for (int i = 0; i < arr.length; i++) {
            if(i % 2 == 0)
                arr[i] += (arr[maxId--] % maxNumber) * maxNumber;
            else
                arr[i] += (arr[minId++] % maxNumber) * maxNumber;
        }
        for (int j = 0; j < arr.length; j++) {
            arr[j] /= maxNumber;
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        maxMin(arr);
    }
}
