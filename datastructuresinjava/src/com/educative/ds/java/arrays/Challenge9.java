package src.com.educative.ds.java.arrays;

/**
 * Challenge 9: Re-arrange Positive & Negative Values
 * Given an array,
 * can you re-arrange its elements in such a way that the negative elements appear at one side and positive elements appear at the other?
 * Solve this problem in Java and see if your code runs correctly!
 * In this problem, you have to implement the void reArrange(int[] arr) method,
 * which will sort the elements, such that all the negative elements appear at the left and positive elements appear at the right.
 * Order of the numbers do not matter.
 */
public class Challenge9 {

    private static void reArrange(int[] arr) {
        int temp = 0;
        for(int left = 0, right = arr.length - 1; left < right;) {
            if(arr[left] >= 0 && arr[right] < 0) {
                temp = arr[right];
                arr[right] = arr[left];
                arr[left] = temp;
                left++;
                right--;
            } else if(arr[left] < 0) left++;
            else {
                left++;
                right--;
            }
        }
        for (int i : arr) {
            System.out.println(i);
        }
    }

    public static void main(String[] args) {
        int[] arr = {10, -1, 20, 4, 5, -9, -6};
        reArrange(arr);
    }
}
