package src.com.educative.ds.java.arrays;

import src.com.educative.ds.java.util.MergeSort;

/**
 * Challenge 3: Find Two Numbers that Add up to "n"
 * Given an array, and a number "n", find two numbers from the array that sums up to "n". Implement your solution in Java and see if your output matches with the correct output.
 */
public class Challenge3 {

    private static int[] findSum(int[] arr, int n) {

        int[] result = new int[2];
        MergeSort.sort(arr, 0, arr.length - 1);
        int sum = 0;

        for(int i = 0, j = arr.length - 1; i < j;) {
            sum = arr[i] + arr[j];
            if(sum < n)
                i++;
            else if(sum > n)
                j--;
            else {
                result[0] = arr[i];
                result[1] = arr[j];
                return result;
            }
        }
        return arr;
    }

    public static void main(String[] args) {
        int[] arr = {1, 21, 3, 14, 5, 60, 7, 6};
        int value = 27;
        findSum(arr, value);
    }
}