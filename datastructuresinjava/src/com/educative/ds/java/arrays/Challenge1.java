package src.com.educative.ds.java.arrays;

/**
 * Challenge 1: Remove Even Integers from an Array
 * Given an array of size n, remove all even integers from it. Implement this solution in Java and see if it runs without an error.
 */
public class Challenge1 {
    private static int[] removeEven(int[] arr) {

        int oddElements = 0;
        for (int value : arr) if (value % 2 != 0) oddElements++;
        int[] result = new int[oddElements];
        for(int i = 0, j = 0; i < arr.length; i++) {
            if (arr[i] % 2 != 0) {
                result[j] = arr[i];
                j++;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 4, 5, 10, 6, 3};
        removeEven(arr);
    }
}
