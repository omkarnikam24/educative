package src.com.educative.ds.java.arrays;

/**
 * Challenge 5: Find Minimum Value in Array
 * Given an array of size "n", can you find the minimum value in the array? Implement your solution in Java and see if your output matches the expected output.
 */
public class Challenge5 {

    private static int findMinimum(int[] arr) {

        int minimumNumber = Integer.MAX_VALUE;
        for(int i = 0, j = arr.length - 1; i <= j; i++, j--) {
            if((arr[i] < arr[j]) && (arr[i] < minimumNumber)) {
                minimumNumber = arr[i];
            } else if((arr[j] < arr[i]) && (arr[j] < minimumNumber)) {
                minimumNumber = arr[j];
            }
        }

        if(arr.length % 2 != 0 && arr[arr.length / 2] < minimumNumber)
            minimumNumber = arr[arr.length / 2];
        return minimumNumber;
    }

    public static void main(String[] args) {
        int[] arr = {9, 2, 0, 6, 5};
        System.out.println(findMinimum(arr));
    }
}
