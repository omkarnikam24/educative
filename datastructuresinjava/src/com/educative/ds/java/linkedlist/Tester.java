package src.com.educative.ds.java.linkedlist;

public class Tester {

    public static void main(String[] args) {

        SinglyLinkedList<Integer> sll = new SinglyLinkedList<>();
        for (int i = 1; i <= 10; i++) {
            //sll.insertAtHead(i);
            sll.insertAtEnd(i);
        }
        //sll.insertAfter(11, 9);
        sll.printList();
        //System.out.println("Searching Node 5, found? : " + sll.searchNode(5));
        //sll.deleteAtHead();
        //sll.deleteByValue(2);
        //sll.printList();
        ReverseLinkedList reverse = new ReverseLinkedList();
        reverse.reverse(sll);
        sll.printList();
        DetectLoop loop = new DetectLoop();
        System.out.println(loop.detectLoop(sll));
        FindMiddleValue mid = new FindMiddleValue();
        System.out.println("Middle Value : " + mid.findMiddle(sll));
        ReturnNthNodeFromEnd ret = new ReturnNthNodeFromEnd();
        System.out.println("4th Element from the end : " + ret.nthElementFromEnd(sll, 4));
    }
}