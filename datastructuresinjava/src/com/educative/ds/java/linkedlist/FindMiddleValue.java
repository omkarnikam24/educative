package src.com.educative.ds.java.linkedlist;

/**
 * Challenge 7: Find the Middle Value of a Linked List
 */
public class FindMiddleValue {

    public <T> Object findMiddle(SinglyLinkedList<T> list) {
        SinglyLinkedList<T>.Node slow = list.headNode;
        SinglyLinkedList<T>.Node fast = list.headNode;

        while (fast.nextNode != null && fast.nextNode.nextNode != null) {
            slow = slow.nextNode;
            fast = fast.nextNode.nextNode;
        }
        return slow.data;
    }
}
