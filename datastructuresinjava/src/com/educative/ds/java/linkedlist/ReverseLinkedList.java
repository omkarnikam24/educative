package src.com.educative.ds.java.linkedlist;

/**
 * Challenge 5: Reverse a Linked List
 */
public class ReverseLinkedList {

    public static <T> void reverse(SinglyLinkedList<T> list) {
        if (list.isEmpty())
            return;

        SinglyLinkedList<T>.Node current = list.headNode;
        SinglyLinkedList<T>.Node next = null;
        SinglyLinkedList<T>.Node newHead = null;

        while (current != null) {
            next = current.nextNode;
            current.nextNode = newHead;
            newHead = current;
            current = next;
        }

        list.headNode = newHead;
    }
}
