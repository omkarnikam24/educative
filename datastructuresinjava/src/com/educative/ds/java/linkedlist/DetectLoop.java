package src.com.educative.ds.java.linkedlist;

/**
 * Challenge 6: Detect Loop in a Linked List
 */
public class DetectLoop {
    public <T> boolean detectLoop(SinglyLinkedList<T> list) {
        SinglyLinkedList<T>.Node slow = list.headNode;
        SinglyLinkedList<T>.Node fast = list.headNode;

        while (slow != null && fast != null && fast.nextNode != null) {
            slow = slow.nextNode;
            fast = fast.nextNode.nextNode;

            if (slow == fast)
                return true;
        }
        return false;
    }
}
