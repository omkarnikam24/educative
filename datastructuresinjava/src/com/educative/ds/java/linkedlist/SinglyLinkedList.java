package src.com.educative.ds.java.linkedlist;

/**
 * This class represents an implementation of a Singly Linked List.
 * Contains Basic LinkedList operations like -
 * 1. Insert at Head
 * 2. Challenge 1: Insertion in a Singly Linked List (Insert at End)
 * 3. Insert After
 * 4. Challenge 2: Search in Singly Linked List.
 * 5. Delete At Head.
 * 6. Challenge 3: Delete By Value.
 * 7. Challenge 4: Find the Length of a Linked List
 */
public class SinglyLinkedList<T> {

    public class Node {
        public T data;
        public Node nextNode;
    }

    public Node headNode;
    public int size;

    public SinglyLinkedList() {
        headNode = null;
        size = 0;
    }

    /**
     * Helper Function to check if List is empty or not
     *
     * @return true if list is empty, else false
     */
    public boolean isEmpty() {
        return null == headNode;
    }

    /**
     * Helper Function to print List
     */
    public void printList() {
        if (isEmpty()) {
            System.out.println("List is Empty!");
            return;
        }

        Node temp = headNode;
        System.out.print("List : ");
        while (temp.nextNode != null) {
            System.out.print(temp.data.toString() + " -> ");
            temp = temp.nextNode;
        }
        System.out.print(temp.data.toString() + " -> null");
    }

    /**
     * Inserts new data at the start of the linked list
     *
     * @param data value to be inserted
     */
    public void insertAtHead(T data) {
        Node newNode = new Node();
        newNode.data = data;
        newNode.nextNode = headNode;
        headNode = newNode;
        size++;
    }

    /**
     * Inserts new data at the end of the linked list
     *
     * @param data value to be inserted
     */
    public void insertAtEnd(T data) {
        if (isEmpty()) {
            insertAtHead(data);
            return;
        }

        Node newNode = new Node();
        newNode.data = data;
        newNode.nextNode = null;

        Node last = headNode;
        while (last.nextNode != null) {
            last = last.nextNode;
        }

        last.nextNode = newNode;
        size++;
    }

    /**
     * Inserts data after the given previous data node
     *
     * @param data     value to be inserted
     * @param previous node after which the value has to be inserted
     */
    public void insertAfter(T data, T previous) {
        Node newNode = new Node();
        newNode.data = data;

        Node currentNode = this.headNode;
        while (currentNode != null && !currentNode.data.equals(previous))
            currentNode = currentNode.nextNode;

        if (currentNode != null) {
            newNode.nextNode = currentNode.nextNode;
            currentNode.nextNode = newNode;
            size++;
        }
    }

    /**
     * Searches a value in the given list
     *
     * @param data value to be searched in the list
     * @return true if value found in the list, false otherwise
     */
    public boolean searchNode(T data) {
        if (null == headNode) return false;
        Node currentNode = headNode;
        while (currentNode != null) {
            if (currentNode.data.equals(data))
                return true;
            currentNode = currentNode.nextNode;
        }
        return false;
    }

    /**
     * Deletes data from the head of list
     */
    public void deleteAtHead() {
        if (isEmpty())
            return;
        headNode = headNode.nextNode;
        size--;
    }

    /**
     * Deletes data given from the linked list
     *
     * @param data - value to be deleted from the list
     */
    public void deleteByValue(T data) {
        if (isEmpty())
            return;
        Node currentNode = headNode;
        Node previousNode = null;
        if (currentNode.data.equals(data)) {
            deleteAtHead();
            return;
        }
        while (currentNode != null) {
            if (currentNode.data.equals(data)) {
                previousNode.nextNode = currentNode.nextNode;
                size--;
                return;
            }
            previousNode = currentNode;
            currentNode = currentNode.nextNode;
        }
    }

    /**
     * Calculates the length of the list.
     *
     * @return Length of the list.
     */
    public int length() {
        int count = 0;
        Node currentNode = headNode;
        while (currentNode != null) {
            count++;
            currentNode = currentNode.nextNode;
        }
        return count;
    }
}