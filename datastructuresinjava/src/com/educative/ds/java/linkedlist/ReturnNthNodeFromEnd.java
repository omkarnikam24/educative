package src.com.educative.ds.java.linkedlist;

/**
 * Challenge 10: Return the Nth node from End
 */
public class ReturnNthNodeFromEnd {

    public <T> Object nthElementFromEnd(SinglyLinkedList<T> list, int n) {

        SinglyLinkedList<T>.Node runner = list.headNode;
        SinglyLinkedList<T>.Node current = list.headNode;
        int count = 0;
        while (runner != null) {
            if (count >= n)
                current = current.nextNode;
            runner = runner.nextNode;
            count++;
        }
        if (n > count)
            return null;
        else
            return current.data;
    }
}
