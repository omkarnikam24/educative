package src.com.educative.ds.java.linkedlist;

import java.util.HashMap;

/**
 * Challenge 9: Union & Intersection of Lists
 * UNION: A Union of two sets can be expressed as, “Union of two sets A and B is a set which contains all the elements present in A or B”.
 * INTERSECTION: The Intersection is commonly defined as, “A set which contains all the elements present in A and B”.
 */
public class UnionAndIntersection {

    public <T> SinglyLinkedList<T> union(SinglyLinkedList<T> list1, SinglyLinkedList<T> list2) {

        if (list1.isEmpty())
            return list2;
        if (list2.isEmpty())
            return list1;

        SinglyLinkedList<T> unionList = new SinglyLinkedList<>();
        SinglyLinkedList<T>.Node current = list1.headNode;

        while (current.nextNode != null) {
            current = current.nextNode;
        }
        current.nextNode = list2.headNode;
        RemoveDuplicates removeDuplicates = new RemoveDuplicates();
        removeDuplicates.removeDuplicates(unionList);
        return unionList;
    }

    public <T> SinglyLinkedList<T> intersection(SinglyLinkedList<T> list1, SinglyLinkedList<T> list2) {

        SinglyLinkedList<T> intersectionList = new SinglyLinkedList<T>();
        if (list1.isEmpty())
            return intersectionList;
        if (list2.isEmpty())
            return intersectionList;

        SinglyLinkedList<T>.Node current = list1.headNode;
        while (current != null) {
            if (contains(list2, current.data))
                intersectionList.insertAtHead(current.data);
            current = current.nextNode;
        }
        return intersectionList;
    }

    private <T> boolean contains(SinglyLinkedList<T> list, T data) {
        SinglyLinkedList<T>.Node current = list.headNode;
        while (current != null) {
            if (current.data.equals(data))
                return true;
            current = current.nextNode;
        }
        return false;
    }
}