package src.com.educative.ds.java.linkedlist;

import java.util.HashSet;

/**
 * Challenge 8: Remove Duplicates from a Linked List
 */
public class RemoveDuplicates {

    public <T> void removeDuplicates(SinglyLinkedList<T> list) {
        HashSet<T> set = new HashSet<>();
        SinglyLinkedList<T>.Node current = list.headNode;
        SinglyLinkedList<T>.Node previous = null;

        while (current != null) {
            if (set.contains(current.data)) {
                previous.nextNode = current.nextNode;
            } else {
                previous = current;
                set.add(current.data);
            }
            current = current.nextNode;
        }
    }
}