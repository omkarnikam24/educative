package com.educative.java8.functionalinterfaces.consumer;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.IntConsumer;

public class ConsumerDemo {

    public static void main(String[] args) {

        Consumer<String> stringConsumer = s -> System.out.println(s);
        stringConsumer.accept("Hello");

        Consumer<Integer> integerConsumer = integer -> System.out.println("Integer value : " + integer);
        integerConsumer.accept(5);

        // Example of andThen()
        Consumer<String> c1 = s -> System.out.println(s + ", My name is Omkar");
        Consumer<String> c2 = s -> System.out.println(s + ", I am from India");
        c1.andThen(c2).accept("Hello");

        // Example of Bi-Consumer
        BiConsumer<String, Integer> biConsumer = (s, integer) -> System.out.println(s + integer);
        biConsumer.accept("Hello, the number is : ", 5);

    }
}
