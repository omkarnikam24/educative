package com.educative.java8.functionalinterfaces.unaryoperator;

import com.educative.java8.functionalinterfaces.comparator.Person;

import java.util.function.UnaryOperator;

public class UnaryOperatorDemo {

    public static void main(String[] args) {
        Person person = new Person();
        UnaryOperator<Person> operator = p -> {
            p.setName("Omkar");
            p.setAge(27);
            p.setCountry("India");
            p.setYearsOfService(5);
            return p;
        };

        operator.apply(person);
        System.out.println(person);
    }
}
