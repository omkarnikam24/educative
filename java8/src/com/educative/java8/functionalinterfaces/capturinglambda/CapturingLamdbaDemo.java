package com.educative.java8.functionalinterfaces.capturinglambda;

import java.util.function.UnaryOperator;

public class CapturingLamdbaDemo {

    public static void main(String[] args) {
        int i = 5;
        //i = 7; // Since we have changed the value of i, the below line will not compile.
        UnaryOperator<Integer> operator = integer -> integer * i;
        System.out.println(operator.apply(i));
    }
}
