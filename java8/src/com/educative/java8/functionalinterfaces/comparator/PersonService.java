package com.educative.java8.functionalinterfaces.comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PersonService {

    public static List<Person> getPersons(List<Person> persons) {

        // Old way, by passing the custom comparator
        Collections.sort(persons, new PersonComparatorOld());

        // New way, using Lambda
        Collections.sort(persons, (p1, p2) -> p1.getName().compareTo(p2.getName()));

        return persons;
    }

    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>();
        persons.add(new Person("Z", 27, "India", 10));
        persons.add(new Person("C", 27, "India", 10));
        persons.add(new Person("D", 27, "India", 10));
        persons.add(new Person("A", 27, "India", 10));
        System.out.println("Input List : " + persons);
        System.out.println(getPersons(persons));
    }
}
