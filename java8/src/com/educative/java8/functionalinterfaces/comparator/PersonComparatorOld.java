package com.educative.java8.functionalinterfaces.comparator;

import com.educative.java8.functionalinterfaces.comparator.Person;

import java.util.Comparator;

public class PersonComparatorOld implements Comparator<Person> {

    @Override
    public int compare(Person p1, Person p2) {
        return p1.getName().compareTo(p2.getName());
    }
}
