package com.educative.java8.functionalinterfaces.comparator;

public class Person {

    private String name;
    private int age;
    private String country;
    private int yearsOfService;

    public Person() {}

    public Person(String name, int age, String country, int yearsOfService) {
        this.name = name;
        this.age = age;
        this.country = country;
        this.yearsOfService = yearsOfService;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getCountry() {
        return country;
    }

    public int getYearsOfService() {
        return yearsOfService;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setYearsOfService(int yearsOfService) {
        this.yearsOfService = yearsOfService;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", country='" + country + '\'' +
                ", yearsOfService=" + yearsOfService +
                '}';
    }
}
