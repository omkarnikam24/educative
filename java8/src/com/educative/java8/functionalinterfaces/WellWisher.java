package com.educative.java8.functionalinterfaces;

public class WellWisher {

    public static void wish(Greeting greeting) {
        greeting.greet();
    }

    public static void main(String[] args) {
        String greeting = "Hello";
        wish(() -> System.out.println(greeting));
    }
}
