package com.educative.java8.functionalinterfaces.predicate;

import com.educative.java8.functionalinterfaces.comparator.Person;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class PredicateDemo {

    // Examples of Predicate
    static boolean isPersonEligibleForVoting(Person person, Predicate<Person> predicate) {
        return predicate.test(person);
    }

    static boolean isPersonEligibleForRetirement(Person person, Predicate<Person> predicate) {
        return predicate.test(person);
    }

    static boolean isNumberLessThanTen(Predicate<Integer> predicate){
        return predicate.negate().test(14);
    }

    // Example of Bi-Predicate
    static boolean isPersonEligibleForVoting(Person person, Integer minAge, BiPredicate<Person, Integer> predicate) {
        return predicate.test(person, minAge);
    }

    public static void main(String[] args) {
        Person person = new Person("Omkar", 50, "India", 31);
        Predicate<Person> greaterThanEighteen = p -> p.getAge() > 18;
        Predicate<Person> lessThanSixty = p -> p.getAge() < 60;
        Predicate<Person> predicate1 = greaterThanEighteen.and(lessThanSixty);

        Predicate<Person> greaterThanSixty = p -> p.getAge() > 60;
        Predicate<Person> serviceMoreThanThirty = p -> p.getYearsOfService() > 30;

        Predicate<Integer> numberGreaterThanTen = p -> p > 10;

        System.out.println("Person is eligible for voting : " + isPersonEligibleForVoting(person, predicate1));
        System.out.println("Person is eligible for retirement : " + isPersonEligibleForVoting(person, greaterThanSixty.or(serviceMoreThanThirty)));
        System.out.println("Number is less than 10 : " + isNumberLessThanTen(numberGreaterThanTen));
        System.out.println("Person is eligible for voting (Bi-Predicate) : " + isPersonEligibleForVoting(person, 21, (p, minAge) -> p.getAge() > minAge));
    }
}
