package com.educative.java8.functionalinterfaces.binaryoperator;

import com.educative.java8.functionalinterfaces.comparator.Person;

import java.util.function.BinaryOperator;

public class BinaryOperatorDemo {

    public static void main(String[] args) {
        Person person1 = new Person("Omkar", 27, "India", 5);
        Person person2 = new Person("Albert", 23, "USA", 2);
        BinaryOperator<Person> operator = (p1, p2) -> {
            p1.setName(p2.getName());
            p1.setAge(p2.getAge());
            p1.setCountry(p2.getCountry());
            p1.setYearsOfService(p2.getYearsOfService());
            return p1;
        };
        operator.apply(person1, person2);
        System.out.println(person1);
    }
}
