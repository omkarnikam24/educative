package com.educative.java8.functionalinterfaces.function;

import java.util.function.BiFunction;

public class BiFunctionInterfaceDemo {

    public static void main(String[] args) {

        BiFunction<Integer, Integer, Integer> biFunction = (a, b) -> a + b;
        System.out.println("Sum : " + biFunction.apply(15, 10));
    }
}
