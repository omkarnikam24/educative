package com.educative.java8.functionalinterfaces.function;

import java.util.function.Function;

public class FunctionInterfaceDemo {

    public static void main(String[] args) {
        Function<String, Integer> lengthFunction = s -> s.length();
        System.out.println("String length : " + lengthFunction.apply("Test string"));

        // Example of - compose(Function<? super V, ? extends T> before)
        Function<Integer, Integer> increment = x -> x + 10;
        Function<Integer, Integer> multiply = y -> y * 2;

        // Since we are using compose(), multiplication will be done first and then increment will be done.
        System.out.println("Compose Result : " + increment.compose(multiply).apply(3));

        // Example of - andThen(Function<? super R,? extends V> after)
        // Since we are using andThen(), increment will be done first and then multiplication will be done.
        System.out.println("andThen Result : " + increment.andThen(multiply).apply(3));
    }
}
