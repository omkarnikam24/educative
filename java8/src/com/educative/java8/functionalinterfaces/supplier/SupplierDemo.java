package com.educative.java8.functionalinterfaces.supplier;

import com.educative.java8.functionalinterfaces.comparator.Person;

import java.util.function.IntSupplier;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class SupplierDemo {

    static boolean isPersonEligibleForVoting(Supplier<Person> supplier, Predicate<Person> predicate) {
        return predicate.test(supplier.get());
    }

    public static void main(String[] args) {

        Supplier<Person> supplier = () -> new Person("Omkar", 27, "India", 5);
        Predicate<Person> predicate = p -> p.getAge() > 18;
        System.out.println("Person is eligible for voting : " + isPersonEligibleForVoting(supplier, predicate));


        // Int Supplier Example
        IntSupplier supplier1 = () -> (int)(Math.random() * 10);
        System.out.println(supplier1.getAsInt());
    }
}
