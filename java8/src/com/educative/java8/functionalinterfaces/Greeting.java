package com.educative.java8.functionalinterfaces;

@FunctionalInterface
public interface Greeting {
    void greet();
}